# Contributing

## Rules

- Be :smile: and :heart:
- Use emoji for the commit messages (see https://gitmoji.carloscuesta.me/)
  - or https://www.webpagefx.com/tools/emoji-cheat-sheet/
