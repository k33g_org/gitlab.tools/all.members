const axios = require("axios")
const fs = require("fs")
const jsonexport = require('jsonexport')

// =========================================
//  How to run this program
// =========================================
// GITLAB_TOKEN_ADMIN="token" GITLAB_URL="https://gitlab.com" node index.js group_path

// =========================================
//  TOOLS
// =========================================
String.prototype.replaceAll = function(search, replacement) {
  var target = this
  return target.split(search).join(replacement)
}

function urlEncodedPath({ name }){
    let encoded = `${name
      .replaceAll("/", "%2F")
      .replaceAll(".", "%2E")
      .replaceAll("-", "%2D")
      .replaceAll("_", "%5F")
      .replaceAll(".", "%2E")}`;
    //console.log("👋", encoded);
    return encoded;
  }

/* access_level
    10 => Guest access
    20 => Reporter access
    30 => Developer access
    40 => Maintainer access
    50 => Owner access # Only valid for groups
*/
function accessLevelLabel({level}) {
  switch(level) {
    case 10:
      return "guest"
      break;
    case 20:
      return "reporter"
      break;
    case 30:
      return "developer"
      break;
    case 40:
      return "maintener"
      break;
    case 50:
      return "owner"
      break;
    default:
      return ""
  }
}

function gitlab({method, path, data}) {
  let token = process.env.GITLAB_TOKEN_ADMIN
  let gitlabUrl = process.env.GITLAB_URL || "https://gitlab.com"
  let headers = {
    "Content-Type": "application/json",
    "Private-Token": token
  }
  return axios({
    method: method,
    url: gitlabUrl + '/api/v4' + path,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}

// =========================================
//  GitLab API
// =========================================

// GET /groups/:id/descendant_groups
// Get descendant subgroups of a group
async function descendantGroupsOfGroup({parentGroupId, perPage, page}) {
  return gitlab({method:"GET", path:`/groups/${parentGroupId}/descendant_groups/?per_page=${perPage}&page=${page}`, data: null})
}

// GET /groups/:id
// details of a group
async function detailsOfgroup({groupId}) {
  return gitlab({method:"GET", path:`/groups/${groupId}`, data: null})
}

// GET /groups/:id/projects
async function projectsOfGroup({groupId, perPage, page}) {
  return gitlab({method:"GET", path:`/groups/${groupId}/projects/?per_page=${perPage}&page=${page}`, data: null})
}

// GET /projects/:id/members/all
async function membersOfProject({projectId, perPage, page}) {
  return gitlab({method:"GET", path:`/projects/${projectId}/members/all?per_page=${perPage}&page=${page}`, data: null})
}

// GET /projects/:id/members
async function onlyDirectMembersOfProject({projectId, perPage, page}) {
  return gitlab({method:"GET", path:`/projects/${projectId}/members/?per_page=${perPage}&page=${page}`, data: null})
}

// =========================================
//  GitLab Helpers
// =========================================

async function get_details_of_group({name}) {
  let groupId = urlEncodedPath({name: name})
  var group = {}, errors = []

  await detailsOfgroup({groupId: groupId})
    .then(response => response.data)
    .then(data => {
      group = data
      return data
    })
    .catch(error => {
      console.log("😡", error.response.statusText, error.response.status, error.response.data)
      errors.push({
        statusText: error.response.statusText,
        status: error.response.status,
        data: error.response.data
      })
    })

  return {result: group, errors: errors}
}

async function get_all_descendant_subgroups_of({name}) {
  var stop = false, page = 1, groups = [], errors = []
  let groupId = urlEncodedPath({name: name})

  console.log(`🤖 fetching descendant subgroups of ${name}`)

  while (!stop) {
    await descendantGroupsOfGroup({parentGroupId: groupId, perPage:10, page: page})
      .then(response => response.data)
      .then(someGroups => {
        if(someGroups.length == 0) { stop = true }
        groups = groups.concat(someGroups)
      })
      .catch(error => {
        console.log("😡", error.response.statusText, error.response.status, error.response.data)
        errors.push({
          statusText: error.response.statusText,
          status: error.response.status,
          data: error.response.data
        })
      })

      page +=1
  }
  return {result: groups, errors: errors}
}

async function get_all_projects_of_group({name}) {
  var stop = false, page = 1, projects = [], errors = []
  let groupId = urlEncodedPath({name: name})

  console.log(`🤖 fetching projects of ${name}`)

  while (!stop) {
    await projectsOfGroup({groupId: groupId, perPage: 10, page: page})
      .then(response => response.data)
      .then(someProjects => {
        if(someProjects.length == 0) { stop = true }
        projects = projects.concat(someProjects)
      })
      .catch(error => {
        console.log("😡", error.response.statusText, error.response.status, error.response.data)
        errors.push({
          statusText: error.response.statusText,
          status: error.response.status,
          data: error.response.data
        })
      })

      page +=1
  }
  return {result: projects, errors: errors}
}


async function get_all_members_of_project({name}) {
  var stop = false, page = 1, members = [], errors = []
  let projectId = urlEncodedPath({name: name})

  console.log(`👨‍🚀 fetching members of ${name}`)

  while (!stop) {
    await membersOfProject({projectId: projectId, perPage: 10, page: page})
      .then(response => response.data)
      .then(someMembers => {
        if(someMembers.length == 0) { stop = true }
        members = members.concat(someMembers)
      })
      .catch(error => {
        console.log("😡", error.response.statusText, error.response.status, error.response.data)
        errors.push({
          statusText: error.response.statusText,
          status: error.response.status,
          data: error.response.data
        })
      })
      page +=1
  }
  return {result: members, errors: errors}
}

// =========================================
//  Main program
// =========================================
async function batch({groupPath}) {
  var allGroups = []
  var allProjects = []
  let parentGroupName = groupPath

  // get the parent group
  let parentGroup = await get_details_of_group({name: parentGroupName})
  // get all descendant groups of the parent group
  let descendantGroups = await get_all_descendant_subgroups_of({name: parentGroupName})

  allGroups = allGroups.concat(
    [parentGroup.result].map(group => group.full_path),
    descendantGroups.result.map(group => group.full_path)
  )

  // you cannot use await inside forEach loop, use for loop instead
  // Parse all groups and get projects list for each group
  for(var group_index in allGroups) {
    let groupFullPath = allGroups[group_index]

    let projectsOfGroup = await get_all_projects_of_group({name: groupFullPath})

    // add members to projects
    for(var project_index in projectsOfGroup.result) {
      let project = projectsOfGroup.result[project_index]
      let membersOfProject = await get_all_members_of_project({name: project.path_with_namespace})
      project.members = membersOfProject.result.map(member => {
        return {
          id: member.id,
          name: member.name,
          username: member.username,
          state: member.state,
          access_level: member.access_level,
          level: accessLevelLabel({level: member.access_level}),
          created_at: member.created_at,
          expires_at: member.expires_at
        }
      })
    }

    // create the json report
    allProjects.push({
      group: groupFullPath,
      projects: projectsOfGroup.result.map(project => {
        return {
          id: project.id,
          name: project.name,
          path_with_namespace: project.path_with_namespace,
          members: project.members
        }
      })
    })

  }
  fs.writeFileSync(`all_projects.json`, JSON.stringify(allProjects, null, 2))

  jsonexport(allProjects, {rowDelimiter: ';'}, (error, csv) => {
    if(error) return console.log("😡", error)
    fs.writeFileSync(`all_projects.csv`, csv)
  })
}

if(process.env.GITLAB_TOKEN_ADMIN==undefined) {
  console.log("👋 you need to set GITLAB_TOKEN_ADMIN")
  process.exit(1)
}
if(process.env.GITLAB_URL==undefined) {
  console.log("👋 you need to set GITLAB_URL")
  process.exit(1)
}

let groupPath = process.argv[2]
if(groupPath==undefined) {
  console.log("👋 you need to provide the path of the group")
  process.exit(1)
}

batch({groupPath: groupPath})

