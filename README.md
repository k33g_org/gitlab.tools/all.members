# all.members

## What

This tool can fetch all members of all projects of all descendant sub groups of a main group. It generates 2 reports:
- `all_projects.json`
- `all_projects.csv`

## Install

- Requirements: nodejs (tested with `v14.15.5`)
- Install: `npm install`

## Run

```bash
GITLAB_TOKEN_ADMIN="token" GITLAB_URL="https://gitlab.com" node index.js group_path
```

> `group_path` is the path and the name of the group (for example: `tanuki-workshops` or `tanuki-workshops/trainings/ci-training/templates`, ...)

## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.